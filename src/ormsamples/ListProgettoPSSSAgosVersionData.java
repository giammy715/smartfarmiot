/**
 * Licensee: 
 * License Type: Evaluation
 */
package ormsamples;

import org.orm.*;
public class ListProgettoPSSSAgosVersionData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing EntityIntrusione...");
		vista_architetturale_gestoresmartfarm.entity2.EntityIntrusione[] vista_Architetturale_GestoreSmartFarmEntity2EntityIntrusiones = vista_architetturale_gestoresmartfarm.entity2.EntityIntrusioneDAO.listEntityIntrusioneByQuery(null, null);
		int length = Math.min(vista_Architetturale_GestoreSmartFarmEntity2EntityIntrusiones.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(vista_Architetturale_GestoreSmartFarmEntity2EntityIntrusiones[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing EntityLettura...");
		vista_architetturale_gestoresmartfarm.entity2.EntityLettura[] vista_Architetturale_GestoreSmartFarmEntity2EntityLetturas = vista_architetturale_gestoresmartfarm.entity2.EntityLetturaDAO.listEntityLetturaByQuery(null, null);
		length = Math.min(vista_Architetturale_GestoreSmartFarmEntity2EntityLetturas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(vista_Architetturale_GestoreSmartFarmEntity2EntityLetturas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing EntityEmployer...");
		vista_architetturale_gestoresmartfarm.entity2.EntityEmployer[] vista_Architetturale_GestoreSmartFarmEntity2EntityEmployers = vista_architetturale_gestoresmartfarm.entity2.EntityEmployerDAO.listEntityEmployerByQuery(null, null);
		length = Math.min(vista_Architetturale_GestoreSmartFarmEntity2EntityEmployers.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(vista_Architetturale_GestoreSmartFarmEntity2EntityEmployers[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing EntitySerra...");
		vista_architetturale_gestoresmartfarm.entity2.EntitySerra[] vista_Architetturale_GestoreSmartFarmEntity2EntitySerras = vista_architetturale_gestoresmartfarm.entity2.EntitySerraDAO.listEntitySerraByQuery(null, null);
		length = Math.min(vista_Architetturale_GestoreSmartFarmEntity2EntitySerras.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(vista_Architetturale_GestoreSmartFarmEntity2EntitySerras[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing EntitySmartFarm...");
		vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarm[] vista_Architetturale_GestoreSmartFarmEntity2EntitySmartFarms = vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarmDAO.listEntitySmartFarmByQuery(null, null);
		length = Math.min(vista_Architetturale_GestoreSmartFarmEntity2EntitySmartFarms.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(vista_Architetturale_GestoreSmartFarmEntity2EntitySmartFarms[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing EntityColtivazione...");
		vista_architetturale_gestoresmartfarm.entity2.EntityColtivazione[] vista_Architetturale_GestoreSmartFarmEntity2EntityColtivaziones = vista_architetturale_gestoresmartfarm.entity2.EntityColtivazioneDAO.listEntityColtivazioneByQuery(null, null);
		length = Math.min(vista_Architetturale_GestoreSmartFarmEntity2EntityColtivaziones.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(vista_Architetturale_GestoreSmartFarmEntity2EntityColtivaziones[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing EntityAmministratore...");
		vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore[] vista_Architetturale_GestoreSmartFarmEntity2EntityAmministratores = vista_architetturale_gestoresmartfarm.entity2.EntityAmministratoreDAO.listEntityAmministratoreByQuery(null, null);
		length = Math.min(vista_Architetturale_GestoreSmartFarmEntity2EntityAmministratores.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(vista_Architetturale_GestoreSmartFarmEntity2EntityAmministratores[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing EntitySensore...");
		vista_architetturale_gestoresmartfarm.entity2.EntitySensore[] vista_Architetturale_GestoreSmartFarmEntity2EntitySensores = vista_architetturale_gestoresmartfarm.entity2.EntitySensoreDAO.listEntitySensoreByQuery(null, null);
		length = Math.min(vista_Architetturale_GestoreSmartFarmEntity2EntitySensores.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(vista_Architetturale_GestoreSmartFarmEntity2EntitySensores[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public static void main(String[] args) {
		try {
			ListProgettoPSSSAgosVersionData listProgettoPSSSAgosVersionData = new ListProgettoPSSSAgosVersionData();
			try {
				listProgettoPSSSAgosVersionData.listTestData();
			}
			finally {
				vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
