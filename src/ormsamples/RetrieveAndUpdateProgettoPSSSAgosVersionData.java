/**
 * Licensee: 
 * License Type: Evaluation
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateProgettoPSSSAgosVersionData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().getSession().beginTransaction();
		try {
			vista_architetturale_gestoresmartfarm.entity2.EntityIntrusione vista_Architetturale_GestoreSmartFarmEntity2EntityIntrusione = vista_architetturale_gestoresmartfarm.entity2.EntityIntrusioneDAO.loadEntityIntrusioneByQuery(null, null);
			// Update the properties of the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntityIntrusioneDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntityIntrusione);
			vista_architetturale_gestoresmartfarm.entity2.EntityLettura vista_Architetturale_GestoreSmartFarmEntity2EntityLettura = vista_architetturale_gestoresmartfarm.entity2.EntityLetturaDAO.loadEntityLetturaByQuery(null, null);
			// Update the properties of the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntityLetturaDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntityLettura);
			vista_architetturale_gestoresmartfarm.entity2.EntityEmployer vista_Architetturale_GestoreSmartFarmEntity2EntityEmployer = vista_architetturale_gestoresmartfarm.entity2.EntityEmployerDAO.loadEntityEmployerByQuery(null, null);
			// Update the properties of the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntityEmployerDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntityEmployer);
			vista_architetturale_gestoresmartfarm.entity2.EntitySerra vista_Architetturale_GestoreSmartFarmEntity2EntitySerra = vista_architetturale_gestoresmartfarm.entity2.EntitySerraDAO.loadEntitySerraByQuery(null, null);
			// Update the properties of the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntitySerraDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntitySerra);
			vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarm vista_Architetturale_GestoreSmartFarmEntity2EntitySmartFarm = vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarmDAO.loadEntitySmartFarmByQuery(null, null);
			// Update the properties of the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarmDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntitySmartFarm);
			vista_architetturale_gestoresmartfarm.entity2.EntityColtivazione vista_Architetturale_GestoreSmartFarmEntity2EntityColtivazione = vista_architetturale_gestoresmartfarm.entity2.EntityColtivazioneDAO.loadEntityColtivazioneByQuery(null, null);
			// Update the properties of the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntityColtivazioneDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntityColtivazione);
			vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore vista_Architetturale_GestoreSmartFarmEntity2EntityAmministratore = vista_architetturale_gestoresmartfarm.entity2.EntityAmministratoreDAO.loadEntityAmministratoreByQuery(null, null);
			// Update the properties of the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntityAmministratoreDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntityAmministratore);
			vista_architetturale_gestoresmartfarm.entity2.EntitySensore vista_Architetturale_GestoreSmartFarmEntity2EntitySensore = vista_architetturale_gestoresmartfarm.entity2.EntitySensoreDAO.loadEntitySensoreByQuery(null, null);
			// Update the properties of the persistent object
			vista_architetturale_gestoresmartfarm.entity2.EntitySensoreDAO.save(vista_Architetturale_GestoreSmartFarmEntity2EntitySensore);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateProgettoPSSSAgosVersionData retrieveAndUpdateProgettoPSSSAgosVersionData = new RetrieveAndUpdateProgettoPSSSAgosVersionData();
			try {
				retrieveAndUpdateProgettoPSSSAgosVersionData.retrieveAndUpdateTestData();
			}
			finally {
				vista_architetturale_gestoresmartfarm.entity2.ProgettoPSSSAgosVersionPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
