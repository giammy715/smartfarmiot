/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

public class EntityColtivazione {
	public EntityColtivazione() {
	}
	
	private int id;
	
	private vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore entityAmministratore;
	
	private vista_architetturale_gestoresmartfarm.entity2.EntitySerra entitySerra;
	
	private java.util.Date dataInizio;
	
	private java.util.Date dataFine;
	
	private String tipologia;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setDataInizio(java.util.Date value) {
		this.dataInizio = value;
	}
	
	public java.util.Date getDataInizio() {
		return dataInizio;
	}
	
	public void setDataFine(java.util.Date value) {
		this.dataFine = value;
	}
	
	public java.util.Date getDataFine() {
		return dataFine;
	}
	
	public void setTipologia(String value) {
		this.tipologia = value;
	}
	
	public String getTipologia() {
		return tipologia;
	}
	
	public void setEntityAmministratore(vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore value) {
		this.entityAmministratore = value;
	}
	
	public vista_architetturale_gestoresmartfarm.entity2.EntityAmministratore getEntityAmministratore() {
		return entityAmministratore;
	}
	
	public void setEntitySerra(vista_architetturale_gestoresmartfarm.entity2.EntitySerra value) {
		this.entitySerra = value;
	}
	
	public vista_architetturale_gestoresmartfarm.entity2.EntitySerra getEntitySerra() {
		return entitySerra;
	}
	
	public String getNotifyInformationAdmin() {
		//TODO: Implement Method
		return getRecapitoAmministratore();
	}
	
	public String getRecapitoAmministratore() {
		//TODO: Implement Method
		return entityAmministratore.getRecapito();
	}
	
	public EntityColtivazione(vista_architetturale_gestoresmartfarm.entity2.EntityColtivazioneDAO coltivazione) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
