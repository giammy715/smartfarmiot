/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

public class EntityEmployer {
	public EntityEmployer() {
	}
	
	public boolean equals(Object aObj) {
		if (aObj == this)
			return true;
		if (!(aObj instanceof EntityEmployer))
			return false;
		EntityEmployer entityemployer = (EntityEmployer)aObj;
		if ((getMatricola() != null && !getMatricola().equals(entityemployer.getMatricola())) || (getMatricola() == null && entityemployer.getMatricola() != null))
			return false;
		return true;
	}
	
	public int hashCode() {
		int hashcode = 0;
		hashcode = hashcode + (getMatricola() == null ? 0 : getMatricola().hashCode());
		return hashcode;
	}
	
	private String matricola;
	
	private String nome;
	
	private String cognome;
	
	private String recapito;
	
	private String tipo;
	
	public void setMatricola(String value) {
		this.matricola = value;
	}
	
	public String getMatricola() {
		return matricola;
	}
	
	public String getORMID() {
		return getMatricola();
	}
	
	public void setNome(String value) {
		this.nome = value;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setCognome(String value) {
		this.cognome = value;
	}
	
	public String getCognome() {
		return cognome;
	}
	
	public void setRecapito(String value) {
		this.recapito = value;
	}
	
	public String getRecapito() {
		return recapito;
	}
	
	public void setTipo(String value) {
		this.tipo = value;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public EntityEmployer(vista_architetturale_gestoresmartfarm.entity2.EntityEmployerDAO responsabile) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getMatricola());
	}
	
}
