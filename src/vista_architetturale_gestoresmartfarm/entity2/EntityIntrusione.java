/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

public class EntityIntrusione {
	public EntityIntrusione() {
	}
	
	private int ID;
	
	private java.util.Date data;
	
	private java.util.Date ora;
	
	private String tipo;
	
	private int[] immagine;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setData(java.util.Date value) {
		this.data = value;
	}
	
	public java.util.Date getData() {
		return data;
	}
	
	public void setOra(java.util.Date value) {
		this.ora = value;
	}
	
	public java.util.Date getOra() {
		return ora;
	}
	
	public void setTipo(String value) {
		this.tipo = value;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setImmagine(int[] value) {
		this.immagine = value;
	}
	
	public int[] getImmagine() {
		return immagine;
	}
	
	public EntityIntrusione(java.util.Date data, java.util.Date ora, String tipo, int[] img) {
		//TODO: Implement Method
		this.data = data;
		this.ora = ora;
		this.tipo = tipo;
		this.immagine = img;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
