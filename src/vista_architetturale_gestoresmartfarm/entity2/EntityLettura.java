/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

public class EntityLettura {
	public EntityLettura() {
	}
	
	private int ID;
	
	private java.util.Date data;
	
	private java.util.Date ora;
	
	private int temperatura;
	
	private int umiditaAria;
	
	private boolean umiditaSuolo;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setData(java.util.Date value) {
		this.data = value;
	}
	
	public java.util.Date getData() {
		return data;
	}
	
	public void setOra(java.util.Date value) {
		this.ora = value;
	}
	
	public java.util.Date getOra() {
		return ora;
	}
	
	public void setTemperatura(int value) {
		this.temperatura = value;
	}
	
	public int getTemperatura() {
		return temperatura;
	}
	
	public void setUmiditaAria(int value) {
		this.umiditaAria = value;
	}
	
	public int getUmiditaAria() {
		return umiditaAria;
	}
	
	public void setUmiditaSuolo(boolean value) {
		this.umiditaSuolo = value;
	}
	
	public boolean getUmiditaSuolo() {
		return umiditaSuolo;
	}
	
	public void salvaLettura() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
