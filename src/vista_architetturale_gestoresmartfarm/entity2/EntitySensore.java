/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

public class EntitySensore {
	public EntitySensore() {
	}
	
	private int ID;
	
	private String tipo;
	
	private float[] valore;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setTipo(String value) {
		this.tipo = value;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setValore(float[] value) {
		this.valore = value;
	}
	
	public float[] getValore() {
		return valore;
	}
	
	public float[] elabora() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public EntitySensore(vista_architetturale_gestoresmartfarm.entity2.EntitySensoreDAO sensore) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
