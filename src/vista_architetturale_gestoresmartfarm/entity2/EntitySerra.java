/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

import java.util.Iterator;

import org.orm.PersistentException;

public class EntitySerra {
	public EntitySerra() {
	}

	private java.util.Set this_getSet (int key) {
		if (key == vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_ENTITYSERRA_ENTITYEMPLOYERS) {
			return ORM_entityEmployers;
		}
		else if (key == vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_ENTITYSERRA_ENTITYSENSORES) {
			return ORM_entitySensores;
		}
		else if (key == vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_ENTITYSERRA_ENTITYINTRUSIONES) {
			return ORM_entityIntrusiones;
		}
		else if (key == vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_ENTITYSERRA_ENTITYLETTURAS) {
			return ORM_entityLetturas;
		}

		return null;
	}

	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}

	};

	private int id;

	private java.util.Set ORM_entityEmployers = new java.util.HashSet();

	private java.util.Set ORM_entitySensores = new java.util.HashSet();

	private java.util.Set ORM_entityIntrusiones = new java.util.HashSet();

	private java.util.Set ORM_entityLetturas = new java.util.HashSet();

	private void setId(int value) {
		this.id = value;
	}

	public int getId() {
		return id;
	}

	public int getORMID() {
		return getId();
	}

	public void setORM_EntityEmployers(java.util.Set value) {
		this.ORM_entityEmployers = value;
	}

	public java.util.Set getORM_EntityEmployers() {
		return ORM_entityEmployers;
	}

	public final vista_architetturale_gestoresmartfarm.entity2.EntityEmployerSetCollection entityEmployers = new vista_architetturale_gestoresmartfarm.entity2.EntityEmployerSetCollection(this, _ormAdapter, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_ENTITYSERRA_ENTITYEMPLOYERS, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_MUL_ONE_TO_MANY);

	public void setORM_EntitySensores(java.util.Set value) {
		this.ORM_entitySensores = value;
	}

	public java.util.Set getORM_EntitySensores() {
		return ORM_entitySensores;
	}

	public final vista_architetturale_gestoresmartfarm.entity2.EntitySensoreSetCollection entitySensores = new vista_architetturale_gestoresmartfarm.entity2.EntitySensoreSetCollection(this, _ormAdapter, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_ENTITYSERRA_ENTITYSENSORES, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_MUL_ONE_TO_MANY);

	public void setORM_EntityIntrusiones(java.util.Set value) {
		this.ORM_entityIntrusiones = value;
	}

	public java.util.Set getORM_EntityIntrusiones() {
		return ORM_entityIntrusiones;
	}

	public final vista_architetturale_gestoresmartfarm.entity2.EntityIntrusioneSetCollection entityIntrusiones = new vista_architetturale_gestoresmartfarm.entity2.EntityIntrusioneSetCollection(this, _ormAdapter, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_ENTITYSERRA_ENTITYINTRUSIONES, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_MUL_ONE_TO_MANY);

	public void setORM_EntityLetturas(java.util.Set value) {
		this.ORM_entityLetturas = value;
	}

	public java.util.Set getORM_EntityLetturas() {
		return ORM_entityLetturas;
	}

	public final vista_architetturale_gestoresmartfarm.entity2.EntityLetturaSetCollection entityLetturas = new vista_architetturale_gestoresmartfarm.entity2.EntityLetturaSetCollection(this, _ormAdapter, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_ENTITYSERRA_ENTITYLETTURAS, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_MUL_ONE_TO_MANY);

	public String getNotifyInformationResponsabile() {
		//TODO: Implement Method
		String recapito = null;
		Iterator<EntityEmployer> it = ORM_entityEmployers.iterator();
		boolean trovato = false;
		while(trovato == false && it.hasNext()){
			EntityEmployer empTemp = it.next();
			if(empTemp.getTipo().equals("responsabile")){
				trovato = true;
				recapito = empTemp.getRecapito(); 
			}
		}

		return recapito;
	}

	public void salvaLettura() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}

	public void salvaLettura2() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}

	public void addIntrusione(java.util.Date data, String tipo, int[] img, java.util.Date ora) {
		//TODO: Implement Method
		EntityIntrusione intrusione = new EntityIntrusione (data,ora,tipo,img);
		//DA TESTARE IMPORTANTISSIMO
		ORM_entityIntrusiones.add(intrusione);
		try {
			EntityIntrusioneDAO.save(intrusione);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public EntitySerra(vista_architetturale_gestoresmartfarm.entity2.EntitySerraDAO serra) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}

	public String[] getNotifyInformationEmployers() {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}

	public vista_architetturale_gestoresmartfarm.entity2.EntitySensore getSensoreProssimita() {
		EntitySensore sensProssimita = null;
		Iterator<EntitySensore> it = ORM_entitySensores.iterator();
		boolean trovato = false;
		while(trovato == false && it.hasNext()){
			EntitySensore sensTemp = it.next();
			if(sensTemp.getTipo().equals("prossimita")){
				trovato = true;
				sensProssimita = sensTemp;
			}

		}
		return sensProssimita;
	}

	public vista_architetturale_gestoresmartfarm.entity2.EntitySensore getSensoreFotografico() {
		EntitySensore sensFotocamera = null;
		Iterator<EntitySensore> it = ORM_entitySensores.iterator();
		boolean trovato = false;
		while(trovato == false && it.hasNext()){
			EntitySensore sensTemp = it.next();
			if(sensTemp.getTipo().equals("cam")){
				trovato = true;
				sensFotocamera = sensTemp;
			}

		}
		return sensFotocamera;
	}

	public vista_architetturale_gestoresmartfarm.entity2.EntityEmployer getResponsabile() {
		//TODO: Implement Method
		EntityEmployer responsabile = null;
		Iterator<EntityEmployer> it = ORM_entityEmployers.iterator();
		boolean trovato = false;
		while(trovato == false && it.hasNext()){
			EntityEmployer empTemp = it.next();
			if(empTemp.getTipo().equals("responsabile")){
				trovato = true;
				responsabile = empTemp; 
			}
		}

		return responsabile;
	}

	public String toString() {
		return String.valueOf(getId());
	}

}
