/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package vista_architetturale_gestoresmartfarm.entity2;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Vector;

import org.orm.PersistentException;

public class EntitySmartFarm {
	private volatile static EntitySmartFarm esm = null;
	
	private EntitySmartFarm() {
		
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_ENTITYSMARTFARM_ENTITYCOLTIVAZIONES) {
			return ORM_entityColtivaziones;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int ID;
	
	private java.util.Set ORM_entityColtivaziones = new java.util.HashSet();
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return this.ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setORM_EntityColtivaziones(java.util.Set value) {
		this.ORM_entityColtivaziones = value;
	}
	
	public java.util.Set getORM_EntityColtivaziones() {
		return this.ORM_entityColtivaziones;
	}
	
	public final vista_architetturale_gestoresmartfarm.entity2.EntityColtivazioneSetCollection entityColtivaziones = new vista_architetturale_gestoresmartfarm.entity2.EntityColtivazioneSetCollection(this, _ormAdapter, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_ENTITYSMARTFARM_ENTITYCOLTIVAZIONES, vista_architetturale_gestoresmartfarm.entity2.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public vista_architetturale_gestoresmartfarm.entity2.EntityColtivazione [] getColtivazioniAttive() {
		//TODO: Implement Method
		EntityColtivazione coltivazioniAttive [] = null;
		Vector<EntityColtivazione> vect = new Vector<EntityColtivazione>();
		Iterator<EntityColtivazione> it =this.ORM_entityColtivaziones.iterator();
		// creazione data odierna per il confronto
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"),Locale.ITALY);
		Date today = calendar.getTime();
		while(it.hasNext()){
			EntityColtivazione coltivazioneTemp = it.next();
			if (coltivazioneTemp.getDataInizio().before(today) && coltivazioneTemp.getDataFine().after(today))
				vect.add(coltivazioneTemp);
		}
		Object[] objTempVett = vect.toArray();
		coltivazioniAttive = Arrays.copyOf(objTempVett, objTempVett.length,EntityColtivazione[].class );
		return coltivazioniAttive;
	}
	
	public void caricaSmartFarm() {
		//TODO: Implement Method
		try {
			this.setORM_EntityColtivaziones(EntitySmartFarmDAO.getEntitySmartFarmByORMID(1).getORM_EntityColtivaziones());
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static public vista_architetturale_gestoresmartfarm.entity2.EntitySmartFarm getInstance() {
		//TODO: Implement Method
		//throw new UnsupportedOperationException();
		if (esm == null) {
			synchronized(EntitySmartFarm.class){
				if(esm == null){
					esm = new EntitySmartFarm();
					esm.caricaSmartFarm();
				}
			}
		}
		return esm;
		
	}
	
	public vista_architetturale_gestoresmartfarm.entity2.EntityColtivazione getColtivazioneBySerraID(int id) {
		//TODO: Implement Method
		throw new UnsupportedOperationException();
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
